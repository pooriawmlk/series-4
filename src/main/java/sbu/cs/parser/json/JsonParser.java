package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class JsonParser
{
    public static Json parse(String data)
    {
        List<jsonTypes> elements = new ArrayList<>();

        data = data.replaceAll("[\\s\"{}]", "");

        if(data.indexOf('[') >= 0)
        {
            data = replaceChar(data);
        }

        String[] segment = data.split(",");

        for(int i = 0; i < segment.length; i++)
        {
            String[] tmp = segment[i].split(":");

            if(tmp[1].matches("\\d*"))
            {
                integerSet(elements, tmp);
            }

            else if(tmp[1].matches("\\d*\\.\\d*"))
            {
                doubleSet(elements, tmp);
            }

            else if(tmp[1].matches("(true|false)"))
            {
                booleanSet(elements, tmp);
            }

            else
            {
                stringSet(elements, tmp);
            }
        }

        return new Json(elements);
    }

    private static String replaceChar(String data)
    {
        String tmp = data;

        while(tmp.indexOf('[') >= 0)
        {
            String str = tmp.substring(tmp.indexOf("["), tmp.indexOf("]")+1);

            data = data.replace(str, str.replaceAll(",", " "));

            tmp = tmp.replace(str, "");
        }

        return data;
    }

    private static List<jsonTypes> integerSet(List<jsonTypes> elements, String[] tmp)
    {
        jsonInteger intValues = new jsonInteger();

        intValues.setKey(tmp[0]);
        intValues.setValue(Integer.parseInt(tmp[1]));

        elements.add(intValues);

        return elements;
    }

    private static List<jsonTypes> doubleSet(List<jsonTypes> elements, String[] tmp)
    {
        jsonDouble doubleValues = new jsonDouble();

        doubleValues.setKey(tmp[0]);
        doubleValues.setValue(Double.parseDouble(tmp[1]));

        elements.add(doubleValues);

        return elements;
    }

    private static List<jsonTypes> stringSet(List<jsonTypes> elements, String[] tmp)
    {
        jsonString stringValues = new jsonString();

        stringValues.setKey(tmp[0]);
        stringValues.setValue(tmp[1]);

        elements.add(stringValues);

        return elements;
    }

    private static List<jsonTypes> booleanSet(List<jsonTypes> elements, String[] tmp)
    {
        jsonBoolean booleanValues = new jsonBoolean();

        booleanValues.setKey(tmp[0]);
        booleanValues.setValue(Boolean.parseBoolean(tmp[1]));

        elements.add(booleanValues);

        return elements;
    }

}
