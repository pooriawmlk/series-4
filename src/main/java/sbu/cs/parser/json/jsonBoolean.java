package sbu.cs.parser.json;

public class jsonBoolean extends jsonTypes
{
    private boolean value;

    @Override
    public String getValue()
    {
        return Boolean.toString(value);
    }

    public void setValue(boolean value)
    {
        this.value = value;
    }
}
