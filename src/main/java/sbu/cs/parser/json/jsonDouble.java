package sbu.cs.parser.json;

public class jsonDouble extends jsonTypes
{
    private double value;

    @Override
    public String getValue()
    {
        return Double.toString(value);
    }

    public void setValue(double value)
    {
        this.value = value;
    }

}
