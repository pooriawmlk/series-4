package sbu.cs.parser.json;

public class jsonInteger extends jsonTypes
{
    private int value;

    @Override
    public String getValue()
    {
        return Integer.toString(value);
    }

    public void setValue(int value)
    {
        this.value = value;
    }
}
