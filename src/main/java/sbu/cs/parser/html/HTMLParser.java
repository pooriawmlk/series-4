package sbu.cs.parser.html;

public class HTMLParser
{
    public static Node parse(String document)
    {
        document = document.replaceAll("[\n]", "");

        Node node = new Node(document);

        return node;
    }

}
