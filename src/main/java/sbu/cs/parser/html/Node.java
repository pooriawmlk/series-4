package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Node implements NodeInterface
{
    private List<Node> children;
    private Map<String, String> attributes;
    private String inside;
    private String document;
    private boolean isSelfClosing;
    private String tagName;

    public Node(String document)
    {
        this.document = document;

        tagName = extractTagName();
        isSelfClosing = extractSelfClosing();
        inside = extractInside();
        attributes = setAttributes();
        children = addChildren();
    }

    private int getOpeningIndex(int start)
    {
        int i = start;

        while(i < inside.length() && inside.charAt(i) != '<')
        {
            i++;
        }

        return i;
    }

    private int getClosingIndex(int start)
    {
        int i = start;

        while(i < inside.length() && inside.charAt(i) != '>')
        {
            i++;
        }

        return i;
    }

    private String getTagName(int currentTagStart, int currentTagEnd)
    {
        int j = currentTagStart+1;

        while(j <= currentTagEnd)
        {
            if(inside.charAt(j) != ' ' && inside.charAt(j) != '>')
            {
                j++;
            }
            else
            {
                break;
            }
        }

        return inside.substring(currentTagStart+1, j);
    }

    private List<Node> addChildren()
    {
        List<Node> child = new ArrayList<>();

        int i = 0;

        while(i < inside.length())
        {
            int currentTagStart = getOpeningIndex(i);

            if(currentTagStart == inside.length())
            {
                return new ArrayList<Node>();
            }

            int currentTagEnd = getClosingIndex(currentTagStart);

            boolean isSelfClosingTag = (inside.charAt(currentTagStart+1) == '/');

            if(isSelfClosingTag)
            {
                child.add(new Node(inside.substring(currentTagStart, currentTagEnd+1)));

                i = currentTagEnd + 1;
            }
            else
            {
                String currentTagName = getTagName(currentTagStart, currentTagEnd);

                int cnt = 1;
                i = currentTagEnd+1;

                while(cnt != 0)
                {
                    int tempTagStart = getOpeningIndex(i);
                    int tempTagEnd = getClosingIndex(tempTagStart);
                    String tempTagName = getTagName(tempTagStart, tempTagEnd);

                    if(currentTagName.equals(tempTagName))
                        cnt++;
                    else if(("/" + currentTagName).equals(tempTagName))
                        cnt--;

                    i = tempTagEnd + 1;
                }

                child.add(new Node (inside.substring(currentTagStart, i)));
            }
        }

        return child;
    }

    private Map<String, String> setAttributes()
    {
        Map<String, String> newAttributes = new HashMap<>();

        int i = 0;

        String insideTag = getTagInside();

        if(insideTag.indexOf('=') < 0)
        {
            return newAttributes;
        }

        while(i < insideTag.length())
        {
            int startOfKey = i;
            int endOfKey;

            while(i < insideTag.length() && insideTag.charAt(i) != '=')
            {
                i++;
            }
            endOfKey = i;

            int startOfValue = endOfKey + 2;
            int endOfValue;

            i = startOfValue;
            while(i < insideTag.length() && insideTag.charAt(i) != '\"')
            {
                i++;
            }
            endOfValue = i;

            newAttributes.put(insideTag.substring(startOfKey, endOfKey),
                    insideTag.substring(startOfValue, endOfValue));
            i += 2;
        }

        if(i == insideTag.length())
        {
            return null;
        }

        return newAttributes;
    }

    private String getTagInside()
    {
        int i = 0;
        int startingPoint = 0;
        int endingPoint = 0;

        while(i < document.length())
        {
            while(i < document.length() && document.charAt(i) != '<')
            {
                i++;
            }

            while(i < document.length() && document.charAt(i) != ' ')
            {
                i++;
            }

            startingPoint = i+1;

            while(i < document.length() && document.charAt(i) != '>')
            {
                i++;
            }
             endingPoint = i;
            break;
        }

        if(startingPoint == endingPoint+1)
        {
            return "";
        }

        return document.substring(startingPoint, endingPoint);
    }

    private String extractInside()
    {
        if(!isSelfClosing)
        {
            int start = 0;
            int end = 0;
            int i = 0;
            while(i < document.length())
            {
                while(i < document.length() && document.charAt(i) != '>')
                {
                    i++;
                }
                start = i+1;

                i = document.length()-2;

                while(i > 0 && document.charAt(i) != '<')
                {
                    i--;
                }
                end = i;

                break;
            }

            return document.substring(start, end);
        }

        return "";
    }

    private boolean extractSelfClosing()
    {
        for(int i = 0; i < document.indexOf(">")-1; i++)
        {
            if(document.charAt(i) == '<')
            {
                if(document.charAt(i+1) == '/')
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        return false;
    }

    private String extractTagName()
    {
        int start = 0;
        int end = 0;

        for(int i = 0; i < document.length()-1; i++)
        {
            if(document.charAt(i) == '<' )
            {
                start = i+1;
                break;
            }
        }

        for(int i = start; i < document.length(); i++)
        {
            if(document.charAt(i) == ' ' || document.charAt(i) == '>')
            {
                end = i;
                break;
            }
        }

        tagName = document.substring(start, end);

        if(tagName.contains("/"))
        {
            tagName = tagName.replace("/", "");
        }

        return tagName;
    }

    @Override
    public String getStringInside()
    {
        if(inside.equals(""))
        {
            return null;
        }

        return inside;
    }

    @Override
    public List<Node> getChildren()
    {
        List<Node> childrenCopy = new ArrayList<>(children);

        return childrenCopy;
    }

    @Override
    public String getAttributeValue(String key)
    {
        return attributes.get(key);
    }
}
